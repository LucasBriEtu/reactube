import React, {useState} from 'react';
import {Container} from 'react-bootstrap';
import MyNav from './components/MyNav';
import Video from "./components/Video";
import DetailedVideo from "./components/DetailedVideo";
import Channel from "./components/Channel";
import DetailedChannel from "./components/DetailedChannel";
import './App.css';

// router
import {BrowserRouter as Router, Switch, Route} from "react-router-dom";

const App = () => {
    const [selectedType, selectType] = useState();
    const [selectedVideo, selectVideo] = useState();
    const [selectedChannel, selectChannel] = useState();
    const [results, setResults] = useState([]);
    return (
        <Container className="p-3">
            <Router>
                <MyNav onResults={setResults} onTypeChanged={selectType} selectedType={selectedType}/>

                <Switch>
                    <Route path="/video/:videoId">
                        {selectedVideo != null && <DetailedVideo id={selectedVideo.id}
                                       snippet={selectedVideo.snippet} player={selectedVideo.player}
                                       statistics={selectedVideo.statistics} />}
                    </Route>
                    <Route path="/search/video/:search">
                        <>
                            {results.map(v => {
                                const {
                                    title, description, thumbnails,
                                    channelTitle, publishTime
                                } = v.snippet;

                                return (<Video
                                    videoId={v.id.videoId}
                                    thumbnail={thumbnails.high}
                                    description={description}
                                    channelTitle={channelTitle}
                                    publishTime={publishTime}
                                    title={title}
                                    selectVideo={selectVideo}/>);
                            })}
                        </>
                    </Route>

                    <Route path="/channel/:channelId">
                        {selectedChannel != null && <DetailedChannel id={selectedChannel.id}
                                       snippet={selectedChannel.snippet}
                                       statistics={selectedChannel.statistics} />}
                    </Route>

                    <Route path="/search/channel/:search">
                        <>
                            {results.map(c => {
                                const {
                                    title, description, thumbnails,
                                    channelTitle, publishTime
                                } = c.snippet;

                                return (<Channel
                                    channelId={c.id.channelId}
                                    thumbnail={thumbnails.high}
                                    description={description}
                                    channelTitle={channelTitle}
                                    publishTime={publishTime}
                                    title={title}
                                    selectChannel={selectChannel}/>);
                            })}
                        </>
                    </Route>
                    <Route path="/">
                        Merci d'effectuer une recherche...
                    </Route>
                </Switch>
            </Router>
        </Container>
    );
};

export default App;
