import axios from 'axios'

const key = 'AIzaSyCqo6Jn9dDAZFgXzPsYyWOLjH0Q7xBUdJU'
const fetcher = axios.create({
    baseURL: 'https://youtube.googleapis.com/youtube/v3',
    params: {
        key: key
    }
})

export default fetcher;
