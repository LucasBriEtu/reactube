import Navbar from "react-bootstrap/Navbar";
import {Nav} from "react-bootstrap";
import SearchBar from './SearchBar';

const MyNav = ({onResults, selectedType, onTypeChanged}) => {
    return (<Navbar bg="dark" variant="dark">
        <Navbar.Brand href="#home">Reactube</Navbar.Brand>
        <Nav className="mr-auto">
            <Nav.Link onClick={()=> onTypeChanged('video')}>Videos</Nav.Link>
            <Nav.Link onClick={()=> onTypeChanged('channel')}>Channels</Nav.Link>
        </Nav>
        <SearchBar onResults={onResults} selectedType={selectedType} />
    </Navbar>);
}

export default MyNav;
